const express = require('express');

const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes')

const app = express();

const port = 4000; 

app.use(express.json())
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin131@bootcamp.y9mz6.mongodb.net/session30?retryWrites=true&w=majority', 
	{

		useNewUrlParser: true,
		useUnifiedTopology: true
	}

);

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Conncection Error"))
	db.once('open', () => console.log('Connected to the cloud database'));

app.use('/tasks', taskRoutes);


app.listen(port, () => console.log(`Server is running at port: ${port}.`));