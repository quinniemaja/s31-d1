//Schema 

const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Task Name is required']
	},
	status: {
		type: String,
		default: "pending"
	}
			
	
});

module.exports = mongoose.model('Task', taskSchema);