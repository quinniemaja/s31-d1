const Task = require('../models/taskSchema')

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	})
}

//

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})
				//.save(err, task)
	return newTask.save().then((task, err) => { // .then(task )
		if(err) {
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {

		if(err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})

}

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr) {

					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
				
			})
	})
}

//Acitivity ======================================================

//get specific id

module.exports.getTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {

		if(err) {
			console.log(err)
			return false
		} else {
			return result
		}
	})
}


//complete task 

module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		}
		result.status = newStatus.status.default
		return result.save().then((updatedStat, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false
			}else {
				return updatedStat
			}
		})
	})
}


